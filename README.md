# FDP@SREC

Materials and Code files for FDP@SREC, by Anandkumar Prakasam

Kindly download the code files in this repository and use them to follow along during the FDP hands-on sessions.
PPT and other materials will be uploaded once the session is complete to this repository itself. 

Code files will be of two types:
1. CYPHER Code Demo - Open using a simple text editor
2. Python Demo Files - Open using Jupyter Notebook

Download Jupyter using Anaconda Distribution for ease of use. 

We are using Python version 2.7. 

use the link to download anaconda for python 2.7 version: 

https://www.anaconda.com/download/ 


For CYPHER use Neo4J desktop with Enterprise for developers edition. 

use the link to download neo4j

https://neo4j.com/download/

Ask the FDP Coordinators for password to unzip the code files...

In case of doubts contact me @ root.anand@gmail.com